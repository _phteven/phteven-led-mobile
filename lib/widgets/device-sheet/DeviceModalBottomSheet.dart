import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:phteven_led_mobile/state/actions.dart';
import 'package:phteven_led_mobile/state/model/AppState.dart';
import 'package:phteven_led_mobile/state/model/Segment.dart';
import 'package:phteven_led_mobile/widgets/dialogs/DeviceColorCorrectionDialog.dart';
import 'package:phteven_led_mobile/widgets/dialogs/DeviceInfoDialog.dart';

import '../../state/model/Device.dart';
import '../dialogs/EditDisplayNameDialog.dart';
import '../ui/GradientSlider.dart';
import 'ColorSegmentControl.dart';

@immutable
class _ViewModel {
  final Device device;
  final Segment segment;

  _ViewModel({required this.device, required this.segment});

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is _ViewModel && runtimeType == other.runtimeType && device == other.device && segment == other.segment;

  @override
  int get hashCode => device.hashCode ^ segment.hashCode;
}

class DeviceModalBottomSheet extends StatefulWidget {
  DeviceModalBottomSheet();

  _DeviceModalBottomSheetState createState() => _DeviceModalBottomSheetState();
}

class _DeviceModalBottomSheetState extends State<DeviceModalBottomSheet> {
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.grey[900],
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(6),
          topRight: Radius.circular(6),
        ),
      ),
      child: Wrap(
        children: this.children(),
      ),
    );
  }

  List<Widget> children() {
    final store = StoreProvider.of<AppState>(context);

    final children = <Widget>[
      Container(
        margin: EdgeInsets.only(left: 10, right: 10, top: 15, bottom: 10),
        child: ColorSegmentControl(),
      ),
      StoreConnector<AppState, _ViewModel>(
        onDispose: (store) {
          final selectedDevice = store.state.selectedDevice;

          if (selectedDevice != null) {
            store.dispatch(SaveSelectedDeviceRequestAction(selectedDevice));
          }
          store.dispatch(DeselectDeviceAction());
        },
        converter: (store) => _ViewModel(
          device: store.state.selectedDevice ?? (throw Exception("Device is null")),
          segment: store.state.selectedDeviceSegment ?? (throw Exception("Segment is null")),
        ),
        builder: (context, viewModel) {
          return Column(
            children: [
              GradientSlider(
                value: viewModel.segment.red,
                thumbColor: Colors.red,
                startColor: Color.fromARGB(255, 0, viewModel.segment.green, viewModel.segment.blue),
                endColor: Color.fromARGB(255, 255, viewModel.segment.green, viewModel.segment.blue),
                onChanged: (value) => store.dispatch(UpdateDeviceSegmentColorAction(value, viewModel.segment.green, viewModel.segment.blue)),
              ),
              GradientSlider(
                value: viewModel.segment.green,
                thumbColor: Colors.green,
                startColor: Color.fromARGB(255, viewModel.segment.red, 0, viewModel.segment.blue),
                endColor: Color.fromARGB(255, viewModel.segment.red, 255, viewModel.segment.blue),
                onChanged: (value) => store.dispatch(UpdateDeviceSegmentColorAction(viewModel.segment.red, value, viewModel.segment.blue)),
              ),
              GradientSlider(
                value: viewModel.segment.blue,
                thumbColor: Colors.blue,
                startColor: Color.fromARGB(255, viewModel.segment.red, viewModel.segment.green, 0),
                endColor: Color.fromARGB(255, viewModel.segment.red, viewModel.segment.green, 255),
                onChanged: (value) => store.dispatch(UpdateDeviceSegmentColorAction(viewModel.segment.red, viewModel.segment.green, value)),
              ),
              GradientSlider(
                value: viewModel.device.brightness,
                thumbColor: Colors.white,
                startColor: Color.fromARGB(255, 0, 0, 0),
                endColor: Color.fromARGB(255, 255, 255, 255),
                onChanged: (value) => store.dispatch(SetDeviceBrightnessAction(viewModel.device.hostname, value)),
              ),
              SizedBox(height: 12),
              Wrap(
                children: viewModel.device.segments
                    .map((e) => Color.fromARGB(255, e.red, e.green, e.blue))
                    .toSet()
                    .map((e) => GestureDetector(
                          onTap: () => store.dispatch(UpdateDeviceSegmentColorAction(e.red, e.green, e.blue)),
                          child: Container(
                            height: 34,
                            width: 34,
                            decoration: BoxDecoration(color: e),
                            margin: EdgeInsets.all(2),
                          ),
                        ))
                    .toList(),
              ),
              Padding(
                padding: EdgeInsets.all(12),
                child: Row(
                  children: [
                    IconButton(
                      onPressed: () {
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return DeviceInfoDialog();
                          },
                        );
                      },
                      icon: Icon(Icons.info, color: Colors.white70),
                    ),
                    IconButton(
                      onPressed: () {
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return DeviceColorCorrectionDialog();
                          },
                        );
                      },
                      icon: Icon(Icons.settings, color: Colors.white70),
                    ),
                    IconButton(
                      onPressed: () {
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return EditDisplayNameDialog();
                          },
                        );
                      },
                      icon: Icon(Icons.mode_edit, color: Colors.white70),
                    ),
                  ],
                ),
              ),
            ],
          );
        },
      )
    ];

    return children.toList();
  }
}

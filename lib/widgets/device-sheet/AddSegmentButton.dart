import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:phteven_led_mobile/state/actions.dart';
import 'package:phteven_led_mobile/state/model/AppState.dart';
import 'package:phteven_led_mobile/state/model/Device.dart';
import 'package:phteven_led_mobile/state/model/Segment.dart';

@immutable
class _ViewModel {
  final Device device;
  final Function addSegment;

  _ViewModel({required this.device, required this.addSegment});

  @override
  bool operator ==(Object other) => identical(this, other) || other is _ViewModel && runtimeType == other.runtimeType && device == other.device;

  @override
  int get hashCode => device.hashCode;
}

class AddSegmentButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      builder: (context, model) => IconButton(
        icon: Icon(Icons.add),
        color: Colors.white70,
        onPressed: model.device.segments.length == Segment.MAX_SEGMENTS ? null : () => model.addSegment(),
      ),
      distinct: true,
      converter: (store) {
        final device = store.state.selectedDevice ?? (throw Exception("Device is null."));

        return _ViewModel(
          device: device,
          addSegment: () => store.dispatch(AddDeviceSegmentAction(device)),
        );
      },
    );
  }
}

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:phteven_led_mobile/extensions.dart';
import 'package:phteven_led_mobile/state/actions.dart';
import 'package:phteven_led_mobile/state/model/AppState.dart';
import 'package:phteven_led_mobile/state/model/Device.dart';
import 'package:phteven_led_mobile/state/model/Segment.dart';

import 'AddSegmentButton.dart';
import 'RemoveSegmentButton.dart';

enum HandlePosition {
  LEFT,
  RIGHT,
}

@immutable
class _ViewModel {
  final Device selectedDevice;
  final Segment selectedDeviceSegment;

  _ViewModel({required this.selectedDevice, required this.selectedDeviceSegment});

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is _ViewModel && runtimeType == other.runtimeType && selectedDevice == other.selectedDevice && selectedDeviceSegment == other.selectedDeviceSegment;

  @override
  int get hashCode => selectedDevice.hashCode ^ selectedDeviceSegment.hashCode;
}

class ColorSegmentControl extends StatelessWidget {
  double _dxStart = 0;
  int _segmentOrignalStart = 0;

  @override
  Widget build(BuildContext context) {
    final store = StoreProvider.of<AppState>(context);

    return StoreConnector<AppState, _ViewModel>(
      converter: (store) => _ViewModel(
        selectedDevice: store.state.selectedDevice ?? (throw Exception("selectedDevice is null")),
        selectedDeviceSegment: store.state.selectedDeviceSegment ?? (throw Exception("selectedDeviceSegment is null")),
      ),
      builder: (context, viewModel) {
        final segments = viewModel.selectedDevice.segments;
        final index = segments.indexOf(viewModel.selectedDeviceSegment);

        if (index == -1) {
          throw Exception("selectedDeviceSegment is not part of segments.");
        }

        return Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Row(
              children: [
                RemoveSegmentButton(),
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                    ),
                    clipBehavior: Clip.antiAlias,
                    child: LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
                      final screenWidth = MediaQuery.of(context).size.width;
                      final maxWidth = constraints.maxWidth;

                      return Row(
                        children: segments.mapIndexed((i, e) {
                          final isSelected = e == viewModel.selectedDeviceSegment;
                          final first = i == 0;
                          final last = i == segments.length - 1;
                          final width = map(e.width().toDouble(), 0, 99, 0, maxWidth);

                          // TODO: make it behave like the slider
                          return GestureDetector(
                            onTap: () => store.dispatch(SelectDeviceSegmentAction(e)),
                            child: _DeviceColorSegment(
                              isSelected: isSelected,
                              segment: e,
                              last: last,
                              first: first,
                              height: 40,
                              width: width,
                              onLeftDragStart: (dx) {
                                _dxStart = dx;
                                _segmentOrignalStart = e.start;
                              },
                              onLeftDragUpdate: (dx) {
                                final diff = dx - _dxStart;
                                final diffMapped = map(diff, 0, screenWidth, 0, 99);
                                final newStart = _segmentOrignalStart + diffMapped;

                                store.dispatch(UpdateDeviceSegmentPositionAction(newStart.toInt(), e.end));
                              },
                              onRightDragStart: (dx) {
                                _dxStart = dx;
                                _segmentOrignalStart = e.end;
                              },
                              onRightDragUpdate: (dx) {
                                final diff = dx - _dxStart;
                                final diffMapped = map(diff, 0, screenWidth, 0, 99);
                                final newEnd = _segmentOrignalStart + diffMapped;

                                store.dispatch(UpdateDeviceSegmentPositionAction(e.start, newEnd.toInt()));
                              },
                            ),
                          );
                        }).toList(),
                      );
                    }),
                  ),
                ),
                AddSegmentButton(),
              ],
            ),
          ],
        );
      },
    );
  }
}

class _DeviceColorSegment extends StatelessWidget {
  final Segment segment;
  final double height;
  final double width;
  final bool first;
  final bool last;
  final bool isSelected;
  final Function(double) onLeftDragStart;
  final Function(double) onLeftDragUpdate;
  final Function(double) onRightDragStart;
  final Function(double) onRightDragUpdate;

  _DeviceColorSegment({
    required this.segment,
    required this.height,
    required this.width,
    required this.first,
    required this.last,
    required this.isSelected,
    required this.onLeftDragStart,
    required this.onLeftDragUpdate,
    required this.onRightDragStart,
    required this.onRightDragUpdate,
  });

  @override
  Widget build(BuildContext context) {
    final leftHandle = first || !isSelected ? SizedBox.shrink() : _handle(position: HandlePosition.LEFT, onDragStart: this.onLeftDragStart, onDragUpdate: this.onLeftDragUpdate);
    final rightHandle = last || !isSelected ? SizedBox.shrink() : _handle(position: HandlePosition.RIGHT, onDragStart: this.onRightDragStart, onDragUpdate: this.onRightDragUpdate);

    final hasLeftBorder = !first;
    final hasRightBorder = !last;

    final borderWidth = 1.0;
    final borderColor = Colors.grey[900]!;

    return Container(
      height: this.height,
      width: this.width,
      clipBehavior: Clip.antiAlias,
      foregroundDecoration: BoxDecoration(
        border: Border(
          right: BorderSide(color: borderColor, width: hasRightBorder ? borderWidth : 0),
          left: BorderSide(color: borderColor, width: hasLeftBorder ? borderWidth : 0),
        ),
      ),
      decoration: BoxDecoration(
        color: Color.fromARGB(255, segment.red, segment.green, segment.blue),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          leftHandle,
          rightHandle,
        ],
      ),
    );
  }

  Widget _handle({required HandlePosition position, required Function(double) onDragStart, required Function(double) onDragUpdate}) {
    final radius = 10.0;
    final left = position == HandlePosition.LEFT;

    return GestureDetector(
      onHorizontalDragStart: isSelected ? (details) => onDragStart(details.globalPosition.dx) : null,
      onHorizontalDragUpdate: isSelected ? (details) => onDragUpdate(details.globalPosition.dx) : null,
      child: Container(
        width: 16,
        height: height,
        child: Stack(
          children: [
            Positioned(
              top: 9,
              left: -4,
              child: Icon(left ? Icons.arrow_left_rounded : Icons.arrow_right_rounded, color: Colors.white70),
            ),
          ],
        ),
        decoration: BoxDecoration(
          color: Colors.grey[900]!.withAlpha(255),
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(!left ? 0 : radius),
            bottomRight: Radius.circular(!left ? 0 : radius),
            topLeft: Radius.circular(left ? 0 : radius),
            bottomLeft: Radius.circular(left ? 0 : radius),
          ),
        ),
      ),
    );
  }
}

double map(double x, double inMin, double inMax, double outMin, double outMax) {
  return (x - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
}

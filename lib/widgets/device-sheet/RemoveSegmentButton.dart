import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:phteven_led_mobile/state/actions.dart';
import 'package:phteven_led_mobile/state/model/AppState.dart';
import 'package:phteven_led_mobile/state/model/Device.dart';

@immutable
class _ViewModel {
  final Device device;
  final Function removeSegment;

  _ViewModel({required this.device, required this.removeSegment});

  @override
  bool operator ==(Object other) => identical(this, other) || other is _ViewModel && runtimeType == other.runtimeType && device == other.device;

  @override
  int get hashCode => device.hashCode;
}

class RemoveSegmentButton extends StatelessWidget {
  static const int MAX_SEGMENTS = 10;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      builder: (context, model) => IconButton(
        icon: Icon(Icons.remove),
        color: Colors.white70,
        onPressed: model.device.segments.length == 1 ? null : () => model.removeSegment(),
      ),
      distinct: true,
      converter: (store) {
        final device = store.state.selectedDevice ?? (throw Exception("Device is null."));

        return _ViewModel(
          device: device,
          removeSegment: () => store.dispatch(RemoveDeviceSegmentAction(device)),
        );
      },
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:phteven_led_mobile/state/actions.dart';
import 'package:phteven_led_mobile/state/model/AppState.dart';
import 'package:redux/redux.dart';

class DeviceErrorNotifier extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      converter: (store) => _ViewModel.fromStore(store),
      builder: (context, vm) => SizedBox.shrink(),
      onWillChange: (previous, model) {
        print("error received");
        if (model.error != null) {
          model.markErrorAsHandled();
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(model.error.toString()),
            ),
          );
        }
      },
      distinct: true,
    );
  }
}

class _ViewModel {
  final Function markErrorAsHandled;
  final Exception? error;

  _ViewModel({
    required this.markErrorAsHandled,
    required this.error,
  });

  static _ViewModel fromStore(Store<AppState> store) {
    return _ViewModel(
      markErrorAsHandled: () => store.dispatch(ClearDeviceHttpErrorAction()),
      error: store.state.deviceHttpError,
    );
  }

  @override
  int get hashCode => error.hashCode;

  @override
  bool operator ==(other) => identical(this, other) || other is _ViewModel && other.error == this.error;
}

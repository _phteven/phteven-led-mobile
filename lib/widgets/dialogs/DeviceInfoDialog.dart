import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:phteven_led_mobile/state/model/AppState.dart';
import 'package:phteven_led_mobile/state/model/Device.dart';

class DeviceInfoDialog extends StatelessWidget {
  Widget build(BuildContext context) {
    return SimpleDialog(
      contentPadding: EdgeInsets.only(left: 15, right: 15, top: 5, bottom: 5),
      children: [
        StoreConnector<AppState, Device>(
          builder: (element, device) {
            return Column(
              children: [
                _infoItem("ID", device.hardwareName ?? "", true),
                _infoItem("WiFi name", device.ssid ?? "", true),
                _infoItem("Current mode", device.isAccessPointMode() ? "Access Point" : "Network", true),
                _infoItem("IP Address", device.address ?? "", true),
                _infoItem("MAC address", device.mac ?? "", false),
              ],
            );
          },
          converter: (store) => store.state.selectedDevice ?? (throw Exception("Device is null")),
        )
      ],
    );
  }

  Widget _infoItem(String label, String value, bool renderBorder) => Container(
        child: Padding(
          padding: EdgeInsets.only(bottom: 15, top: 15),
          child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text(label, textAlign: TextAlign.start, style: TextStyle(fontSize: 14)),
            Text(value, textAlign: TextAlign.right, style: TextStyle(fontSize: 14)),
          ]),
        ),
        decoration: BoxDecoration(border: renderBorder ? Border(bottom: BorderSide(width: 1, color: Colors.grey[850]!)) : null),
      );
}

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:phteven_led_mobile/state/actions.dart';
import 'package:phteven_led_mobile/state/model/AppState.dart';
import 'package:phteven_led_mobile/widgets/ui/GradientSlider.dart';

class _ViewModel {
  final int red;
  final int green;
  final int blue;

  _ViewModel({required this.red, required this.green, required this.blue});

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is _ViewModel && runtimeType == other.runtimeType && red == other.red && green == other.green && blue == other.blue;

  @override
  int get hashCode => red.hashCode ^ green.hashCode ^ blue.hashCode;
}

class DeviceColorCorrectionDialog extends StatelessWidget {
  Widget build(BuildContext context) {
    return SimpleDialog(
      contentPadding: EdgeInsets.only(left: 5, right: 5, top: 15, bottom: 0),
      children: [
        StoreConnector<AppState, _ViewModel>(
          builder: (context, model) {
            final store = StoreProvider.of<AppState>(context);

            return Column(
              children: [
                Text("Calibrate color", style: TextStyle(color: Colors.white70)),
                GradientSlider(
                  value: model.red,
                  thumbColor: Colors.red,
                  startColor: Color.fromARGB(255, 0, 0, 0),
                  endColor: Color.fromARGB(255, 255, 0, 0),
                  onChanged: (value) => store.dispatch(
                    SetDeviceColorCorrectionAction(
                      red: value,
                      green: model.green,
                      blue: model.blue,
                    ),
                  ),
                ),
                GradientSlider(
                  value: model.green,
                  thumbColor: Colors.green,
                  startColor: Color.fromARGB(255, 0, 0, 0),
                  endColor: Color.fromARGB(255, 0, 255, 0),
                  onChanged: (value) => store.dispatch(
                    SetDeviceColorCorrectionAction(
                      red: model.red,
                      green: value,
                      blue: model.blue,
                    ),
                  ),
                ),
                GradientSlider(
                  value: model.blue,
                  thumbColor: Colors.blue,
                  startColor: Color.fromARGB(255, 0, 0, 0),
                  endColor: Color.fromARGB(255, 0, 0, 255),
                  onChanged: (value) => store.dispatch(
                    SetDeviceColorCorrectionAction(
                      red: model.red,
                      green: model.green,
                      blue: value,
                    ),
                  ),
                ),
                ButtonBar(
                  children: [
                    TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text("Cancel"),
                    ),
                  ],
                )
              ],
            );
          },
          converter: (store) {
            return _ViewModel(
              red: store.state.deviceColorCorrectionForm.red,
              green: store.state.deviceColorCorrectionForm.green,
              blue: store.state.deviceColorCorrectionForm.blue,
            );
          },
        ),
      ],
    );
  }
}

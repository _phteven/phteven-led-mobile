import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:phteven_led_mobile/state/actions.dart';
import 'package:phteven_led_mobile/state/model/AppState.dart';

class _ViewModel {
  final String displayName;
  final Function(String) setText;
  final Function save;
  final Function cancel;

  _ViewModel({required this.displayName, required this.setText, required this.save, required this.cancel});

  @override
  bool operator ==(Object other) => identical(this, other) || other is _ViewModel && runtimeType == other.runtimeType && displayName == other.displayName;

  @override
  int get hashCode => displayName.hashCode;
}

class EditDisplayNameDialog extends StatelessWidget {
  final TextEditingController _textEditingController = TextEditingController();

  Widget build(BuildContext context) {
    return SimpleDialog(
      contentPadding: EdgeInsets.only(left: 15, right: 15, top: 15),
      children: [
        StoreConnector<AppState, _ViewModel>(
          onInitialBuild: (model) => _textEditingController.text = model.displayName,
          builder: (context, model) {
            return Column(
              children: [
                TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Name',
                  ),
                  controller: _textEditingController,
                  onChanged: (value) => model.setText(value),
                ),
                ButtonBar(
                  children: [
                    TextButton(
                      onPressed: () {
                        model.cancel();
                        Navigator.pop(context);
                      },
                      child: Text("Cancel"),
                    ),
                    TextButton(
                      onPressed: model.displayName == ""
                          ? null
                          : () {
                              model.save();
                              Navigator.pop(context);
                            },
                      child: Text("Save"),
                    ),
                  ],
                )
              ],
            );
          },
          converter: (store) {
            return _ViewModel(
              displayName: store.state.deviceDisplayNameForm.displayName,
              setText: (value) => store.dispatch(UpdateDeviceDisplayFormValue(value)),
              cancel: () => store.dispatch(CancelDeviceDisplayNameFormAction()),
              save: () => store.dispatch(SaveDeviceDisplayNameFormAction()),
            );
          },
        ),
      ],
    );
  }
}

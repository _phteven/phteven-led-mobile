import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:phteven_led_mobile/state/actions.dart';
import 'package:phteven_led_mobile/state/model/AppState.dart';
import 'package:phteven_led_mobile/widgets/device-card/DeviceCard.dart';

import 'DeviceErrorNotifier.dart';

class DevicesListPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final store = StoreProvider.of<AppState>(context);

    return Scaffold(
      floatingActionButton: StoreConnector<AppState, bool>(
        converter: (store) => store.state.deviceLoaderVisible,
        distinct: true,
        builder: (context, searchingDevices) {
          return FloatingActionButton(
            onPressed: searchingDevices ? null : () => store.dispatch(ScanDevicesRequestAction(true)),
            child: Icon(Icons.refresh),
          );
        },
      ),
      body: SafeArea(
        child: Container(
          color: Colors.grey[850],
          child: Align(
            alignment: Alignment.topCenter,
            child: Column(
              children: [
                DeviceErrorNotifier(),
                StoreConnector<AppState, bool>(
                  // todo: put into own widget
                  converter: (store) => store.state.deviceLoaderVisible,
                  distinct: true,
                  builder: (context, searchingDevices) {
                    if (searchingDevices) {
                      return Padding(
                        padding: EdgeInsets.all(20),
                        child: CircularProgressIndicator(),
                      );
                    }

                    return SizedBox.shrink();
                  },
                ),
                // todo: put into own widget
                SingleChildScrollView(
                  child: Padding(
                    padding: EdgeInsets.all(10),
                    child: StoreConnector<AppState, AppState>(
                      distinct: true,
                      converter: (store) => store.state,
                      builder: (context, state) {
                        if (state.deviceLoaderVisible || state.devices.length > 0) {
                          return Column(children: state.devices.map((device) => DeviceCard(device: device)).toList());
                        } else {
                          return Row(
                            children: [
                              Expanded(
                                child: Card(
                                  color: Colors.grey[900],
                                  child: Padding(
                                    padding: EdgeInsets.all(15),
                                    child: Text(
                                      "No devices here. Scan again to find devices.",
                                      style: TextStyle(color: Colors.white70),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          );
                        }
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

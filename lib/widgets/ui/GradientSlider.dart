import 'dart:math';

import 'package:flutter/material.dart';

import 'GradientRectSliderTrackShape.dart';

class GradientSlider extends StatelessWidget {
  final int value;
  final Color startColor;
  final Color endColor;
  final Function(int value) onChanged;
  final Color thumbColor;

  const GradientSlider({
    Key? key,
    required this.value,
    required this.startColor,
    required this.endColor,
    required this.onChanged,
    required this.thumbColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliderTheme(
      data: SliderTheme.of(context).copyWith(
        trackShape: GradientRectSliderTrackShape(gradient: LinearGradient(colors: [startColor, endColor]), darkenInactive: false),
        trackHeight: 12.0,
        thumbShape: RoundSliderThumbShape(enabledThumbRadius: 15),
        thumbColor: thumbColor,
        overlayShape: RoundSliderOverlayShape(overlayRadius: 28),
        overlayColor: Colors.white70,
      ),
      child: Slider(
        min: 0,
        max: 255,
        value: value.toDouble(),
        onChanged: (value) {
          onChanged(value.toInt());
        },
      ),
    );
  }
}

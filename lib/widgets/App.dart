import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:phteven_led_mobile/state/actions.dart';
import 'package:phteven_led_mobile/state/model/AppState.dart';
import 'package:redux/redux.dart';

import 'DevicesListPage.dart';

class App extends StatelessWidget {
  final Store<AppState> store;

  App({Key? key, required this.store}) : super(key: key) {
    store.dispatch(FetchCachedDevicesRequestAction());
    store.dispatch(FetchFavoriteColorsRequestAction());
    store.dispatch(ScanDevicesRequestAction(false));
  }

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        theme: ThemeData.dark(),
        title: "phteven-led",
        home: DevicesListPage(),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:phteven_led_mobile/state/actions.dart';
import 'package:phteven_led_mobile/state/model/AppState.dart';
import 'package:phteven_led_mobile/state/model/Device.dart';

class DeviceCardTitle extends StatelessWidget {
  final Device _device;

  DeviceCardTitle({required Device device}) : _device = device;

  @override
  Widget build(BuildContext context) {
    final store = StoreProvider.of<AppState>(context);

    final right = _device.isOnline
        ? Switch(
            value: !_device.isDark,
            onChanged: (bool value) => store.dispatch(SetDeviceBrightnessAction(_device.hostname, value ? 255 : 0)),
          )
        : IconButton(
            icon: Icon(Icons.delete_outline_rounded, color: Colors.white70),
            onPressed: () => store.dispatch(RemoveDeviceAction(_device)),
          );

    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          child: Text(_device.getTitle(), style: TextStyle(color: Colors.white70)),
          decoration: BoxDecoration(color: Colors.grey[800], borderRadius: BorderRadius.all(Radius.circular(5))),
          padding: EdgeInsets.all(12),
        ),
        Spacer(),
        right,
      ],
    );
  }
}

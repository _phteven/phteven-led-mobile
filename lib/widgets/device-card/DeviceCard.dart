import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:phteven_led_mobile/extensions.dart';
import 'package:phteven_led_mobile/state/actions.dart';
import 'package:phteven_led_mobile/state/model/AppState.dart';

import '../../state/model/Device.dart';
import '../device-sheet/DeviceModalBottomSheet.dart';
import 'DeviceCardTitle.dart';

class DeviceCard extends StatelessWidget {
  final Device device;

  DeviceCard({Key? key, required this.device});

  @override
  Widget build(BuildContext context) {
    final store = StoreProvider.of<AppState>(context);

    return GestureDetector(
      onTap: () {
        if (device.isOnline) {
          store.dispatch(SelectDeviceAction(device));
          showModalBottomSheet(
            backgroundColor: Colors.transparent,
            isScrollControlled: true,
            context: context,
            builder: (context) => DeviceModalBottomSheet(),
          );
        }
      },
      child: Opacity(
        opacity: device.isOnline ? 1 : 0.5,
        child: Card(
          color: Colors.grey[900],
          clipBehavior: Clip.antiAlias,
          child: Column(children: [
            Padding(
              padding: EdgeInsets.all(10),
              child: DeviceCardTitle(device: device),
            ),
            Row(
                children: device.segments
                    .mapIndexed((i, e) => Expanded(
                          flex: e.width(),
                          child: Container(
                            height: 18,
                            decoration: BoxDecoration(
                              color: Color.fromARGB(device.isDark ? 50 : 255, e.red, e.green, e.blue),
                              border: Border(right: BorderSide(color: Colors.grey[900]!, width: i != device.segments.length - 1 ? 2 : 0)),
                            ),
                          ),
                        ))
                    .toList()),
          ]),
        ),
      ),
    );
  }
}

import 'dart:io';

import 'package:multicast_dns/multicast_dns.dart';
import 'package:phteven_led_mobile/state/model/Device.dart';

import 'AbstractMdnsEngine.dart';

class MdnsEngine implements AbstractMdnsEngine {
  final String name;

  MdnsEngine({required this.name});

  Future<Set<Device>> scanDevices() async {
    final RawDatagramSocketFactory _factory = (dynamic host, int port, {bool reuseAddress = true, bool reusePort = false, int ttl = 1}) {
      return RawDatagramSocket.bind(host, port, reuseAddress: true, reusePort: false, ttl: ttl);
    };

    final client = MDnsClient(rawDatagramSocketFactory: _factory);

    await client.start();
    final devices = Set<Device>();
    await for (var ptr in client.lookup<PtrResourceRecord>(ResourceRecordQuery.serverPointer(name))) {
      await for (var srv in client.lookup<SrvResourceRecord>(ResourceRecordQuery.service(ptr.domainName))) {
        await for (var ip in client.lookup<IPAddressResourceRecord>(ResourceRecordQuery.addressIPv4(srv.target))) {
          devices.add(
            Device(
              hostname: srv.target,
              address: ip.address.address,
              port: srv.port,
              redCorrection: 0,
              greenCorrection: 0,
              blueCorrection: 0,
            ),
          );
        }
      }
    }

    client.stop();

    return devices;
  }
}

import 'package:phteven_led_mobile/state/model/Device.dart';

import 'AbstractMdnsEngine.dart';

class MdnsService {
  final AbstractMdnsEngine engine;

  MdnsService({required this.engine});

  Future<Set<Device>> scanDevices() async {
    return engine.scanDevices();
  }
}

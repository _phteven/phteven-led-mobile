import 'package:phteven_led_mobile/state/model/Device.dart';
import 'package:phteven_led_mobile/state/model/Segment.dart';

import 'AbstractMdnsEngine.dart';

class MockedDnsEngine implements AbstractMdnsEngine {
  Future<Set<Device>> scanDevices() async {
    return Future.delayed(const Duration(milliseconds: 2000), () {
      return {
        Device(
          hostname: "phteven-led-1.local",
          address: "127.0.0.1",
          port: 80,
          displayName: "TV Backlight",
          brightness: 255,
          mode: Device.DeviceModeNetwork,
          ssid: "Wlan SSID",
          mac: "FF:FF:FF:FF:FF:FF",
          hardwareName: "phteven-led-1",
          redCorrection: 0,
          greenCorrection: 0,
          blueCorrection: 0,
          segments: [
            Segment(
              red: 255,
              green: 60,
              blue: 0,
              start: 0,
              end: 99,
            ),
          ],
          version: 0,
        ),
        Device(
          hostname: "phteven-led-2.local",
          address: "127.0.0.1",
          port: 80,
          displayName: "Speaker Left",
          brightness: 255,
          mode: Device.DeviceModeNetwork,
          ssid: "Wlan SSID",
          mac: "FF:FF:FF:FF:FF:FF",
          hardwareName: "phteven-led-2",
          redCorrection: 0,
          greenCorrection: 0,
          blueCorrection: 0,
          segments: [
            Segment(
              red: 255,
              green: 60,
              blue: 0,
              start: 0,
              end: 99,
            ),
          ],
          version: 0,
        ),
        Device(
          hostname: "phteven-led-3.local",
          address: "127.0.0.1",
          port: 80,
          displayName: "Speaker Right",
          brightness: 255,
          mode: Device.DeviceModeNetwork,
          ssid: "Wlan SSID",
          mac: "FF:FF:FF:FF:FF:FF",
          hardwareName: "phteven-led-3",
          redCorrection: 0,
          greenCorrection: 0,
          blueCorrection: 0,
          segments: [
            Segment(
              red: 255,
              green: 60,
              blue: 0,
              start: 0,
              end: 99,
            ),
          ],
          version: 0,
        ),
        Device(
          hostname: "phteven-led-4.local",
          displayName: "Bedroom",
          redCorrection: 0,
          greenCorrection: 0,
          blueCorrection: 0,
        ),
      };
    });
  }
}

import 'package:phteven_led_mobile/state/model/Device.dart';

abstract class AbstractMdnsEngine {
  Future<Set<Device>> scanDevices();
}

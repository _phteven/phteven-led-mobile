import 'package:phteven_led_mobile/state/model/Device.dart';
import 'package:throttling/throttling.dart';

import 'api/DeviceHttpClient.dart';

class UpdateDeviceColorService {
  final Function(Device, int) _onUpdate;
  final Function(Exception) _onError;
  final Map<String, _DeviceUpdateColorThrottling> _updater = {};

  UpdateDeviceColorService(this._onUpdate, this._onError);

  void update(Device device) {
    if (_updater[device.hostname] == null) {
      _updater[device.hostname] = _DeviceUpdateColorThrottling(_onUpdate, _onError);
    }

    _updater[device.hostname]?.update(device);
  }
}

class _DeviceUpdateColorThrottling {
  final Function(Device, int) _onUpdate;
  final Function(Exception) _onError;
  final _updateColorThrottle = new Throttling(duration: ThrottlingDuration);
  Device? _device;

  static const ThrottlingDuration = Duration(milliseconds: 60);

  _DeviceUpdateColorThrottling(this._onUpdate, this._onError) {
    _updateColorThrottle.listen((ready) async {
      final device = this._device;
      if (ready && device != null) {
        try {
          await DeviceHttpClient.putColor(device).then((version) => _onUpdate(device, version));
        } on Exception catch (e) {
          _onError(e);
        }
      }
    });
  }

  void update(Device device) {
    _device = device;
    _updateColorThrottle.throttle(() => null);
  }
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

// ignore: avoid_classes_with_only_static_members
class $FloorAppDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder databaseBuilder(String name) =>
      _$AppDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$AppDatabaseBuilder(null);
}

class _$AppDatabaseBuilder {
  _$AppDatabaseBuilder(this.name);

  final String? name;

  final List<Migration> _migrations = [];

  Callback? _callback;

  /// Adds migrations to the builder.
  _$AppDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$AppDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<AppDatabase> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name!)
        : ':memory:';
    final database = _$AppDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$AppDatabase extends AppDatabase {
  _$AppDatabase([StreamController<String>? listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  DeviceRepository? _deviceDaoInstance;

  FavoriteColorRepository? _favoriteColorDaoInstance;

  Future<sqflite.Database> open(String path, List<Migration> migrations,
      [Callback? callback]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
        await callback?.onConfigure?.call(database);
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `Device` (`host_name` TEXT NOT NULL, `brightness` INTEGER NOT NULL, `address` TEXT, `port` INTEGER, `hardware_name` TEXT, `display_name` TEXT, `mode` TEXT, `ssid` TEXT, `mac` TEXT, `red_correction` INTEGER NOT NULL, `green_correction` INTEGER NOT NULL, `blue_correction` INTEGER NOT NULL, PRIMARY KEY (`host_name`))');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `FavoriteColor` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `red` INTEGER NOT NULL, `green` INTEGER NOT NULL, `blue` INTEGER NOT NULL)');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  DeviceRepository get deviceDao {
    return _deviceDaoInstance ??= _$DeviceRepository(database, changeListener);
  }

  @override
  FavoriteColorRepository get favoriteColorDao {
    return _favoriteColorDaoInstance ??=
        _$FavoriteColorRepository(database, changeListener);
  }
}

class _$DeviceRepository extends DeviceRepository {
  _$DeviceRepository(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _deviceInsertionAdapter = InsertionAdapter(
            database,
            'Device',
            (Device item) => <String, Object?>{
                  'host_name': item.hostname,
                  'brightness': item.brightness,
                  'address': item.address,
                  'port': item.port,
                  'hardware_name': item.hardwareName,
                  'display_name': item.displayName,
                  'mode': item.mode,
                  'ssid': item.ssid,
                  'mac': item.mac,
                  'red_correction': item.redCorrection,
                  'green_correction': item.greenCorrection,
                  'blue_correction': item.blueCorrection
                }),
        _deviceUpdateAdapter = UpdateAdapter(
            database,
            'Device',
            ['host_name'],
            (Device item) => <String, Object?>{
                  'host_name': item.hostname,
                  'brightness': item.brightness,
                  'address': item.address,
                  'port': item.port,
                  'hardware_name': item.hardwareName,
                  'display_name': item.displayName,
                  'mode': item.mode,
                  'ssid': item.ssid,
                  'mac': item.mac,
                  'red_correction': item.redCorrection,
                  'green_correction': item.greenCorrection,
                  'blue_correction': item.blueCorrection
                }),
        _deviceDeletionAdapter = DeletionAdapter(
            database,
            'Device',
            ['host_name'],
            (Device item) => <String, Object?>{
                  'host_name': item.hostname,
                  'brightness': item.brightness,
                  'address': item.address,
                  'port': item.port,
                  'hardware_name': item.hardwareName,
                  'display_name': item.displayName,
                  'mode': item.mode,
                  'ssid': item.ssid,
                  'mac': item.mac,
                  'red_correction': item.redCorrection,
                  'green_correction': item.greenCorrection,
                  'blue_correction': item.blueCorrection
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<Device> _deviceInsertionAdapter;

  final UpdateAdapter<Device> _deviceUpdateAdapter;

  final DeletionAdapter<Device> _deviceDeletionAdapter;

  @override
  Future<List<Device>> findAll() async {
    return _queryAdapter.queryList('SELECT * FROM Device',
        mapper: (Map<String, Object?> row) => Device(
            hostname: row['host_name'] as String,
            brightness: row['brightness'] as int?,
            address: row['address'] as String?,
            port: row['port'] as int?,
            displayName: row['display_name'] as String?,
            hardwareName: row['hardware_name'] as String?,
            mode: row['mode'] as String?,
            ssid: row['ssid'] as String?,
            mac: row['mac'] as String?,
            redCorrection: row['red_correction'] as int,
            greenCorrection: row['green_correction'] as int,
            blueCorrection: row['blue_correction'] as int));
  }

  @override
  Future<int> insert(Device entity) {
    return _deviceInsertionAdapter.insertAndReturnId(
        entity, OnConflictStrategy.abort);
  }

  @override
  Future<void> update(Device entity) async {
    await _deviceUpdateAdapter.update(entity, OnConflictStrategy.replace);
  }

  @override
  Future<int> remove(Device entity) {
    return _deviceDeletionAdapter.deleteAndReturnChangedRows(entity);
  }
}

class _$FavoriteColorRepository extends FavoriteColorRepository {
  _$FavoriteColorRepository(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _favoriteColorInsertionAdapter = InsertionAdapter(
            database,
            'FavoriteColor',
            (FavoriteColor item) => <String, Object?>{
                  'id': item.id,
                  'red': item.red,
                  'green': item.green,
                  'blue': item.blue
                }),
        _favoriteColorUpdateAdapter = UpdateAdapter(
            database,
            'FavoriteColor',
            ['id'],
            (FavoriteColor item) => <String, Object?>{
                  'id': item.id,
                  'red': item.red,
                  'green': item.green,
                  'blue': item.blue
                }),
        _favoriteColorDeletionAdapter = DeletionAdapter(
            database,
            'FavoriteColor',
            ['id'],
            (FavoriteColor item) => <String, Object?>{
                  'id': item.id,
                  'red': item.red,
                  'green': item.green,
                  'blue': item.blue
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<FavoriteColor> _favoriteColorInsertionAdapter;

  final UpdateAdapter<FavoriteColor> _favoriteColorUpdateAdapter;

  final DeletionAdapter<FavoriteColor> _favoriteColorDeletionAdapter;

  @override
  Future<List<FavoriteColor>> findAll() async {
    return _queryAdapter.queryList('SELECT * FROM FavoriteColor',
        mapper: (Map<String, Object?> row) => FavoriteColor(row['id'] as int,
            row['red'] as int, row['green'] as int, row['blue'] as int));
  }

  @override
  Future<int> insert(FavoriteColor entity) {
    return _favoriteColorInsertionAdapter.insertAndReturnId(
        entity, OnConflictStrategy.abort);
  }

  @override
  Future<void> update(FavoriteColor entity) async {
    await _favoriteColorUpdateAdapter.update(
        entity, OnConflictStrategy.replace);
  }

  @override
  Future<int> remove(FavoriteColor entity) {
    return _favoriteColorDeletionAdapter.deleteAndReturnChangedRows(entity);
  }
}

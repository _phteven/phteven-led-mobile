import 'package:floor/floor.dart';
import 'package:phteven_led_mobile/state/model/FavoriteColor.dart';

@dao
abstract class FavoriteColorRepository {
  @Query('SELECT * FROM FavoriteColor')
  Future<List<FavoriteColor>> findAll();

  @Insert()
  Future<int> insert(FavoriteColor entity);

  @delete
  Future<int> remove(FavoriteColor entity);

  @Update(onConflict: OnConflictStrategy.replace)
  Future<void> update(FavoriteColor entity);
}

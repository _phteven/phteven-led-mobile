import 'package:phteven_led_mobile/state/model/FavoriteColor.dart';

import 'FavoriteColorRepository.dart';

class InMemoryFavoriteColorRepository extends FavoriteColorRepository {
  @override
  Future<List<FavoriteColor>> findAll() async {
    return List.empty(growable: false);
  }

  @override
  Future<int> insert(FavoriteColor entity) async {
    return 0;
  }

  @override
  Future<int> remove(FavoriteColor entity) async {
    return 0;
  }

  @override
  Future<void> update(FavoriteColor entity) async {}
}

import 'package:phteven_led_mobile/state/model/Device.dart';

import 'DeviceRepository.dart';

class InMemoryDeviceRepository extends DeviceRepository {
  @override
  Future<List<Device>> findAll() async {
    return List.empty(growable: false);
  }

  @override
  Future<int> insert(Device device) async {
    return 0;
  }

  @override
  Future<int> remove(Device device) async {
    return 0;
  }

  @override
  Future<void> update(Device device) async {}
}

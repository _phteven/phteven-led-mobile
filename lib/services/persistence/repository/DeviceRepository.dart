import 'package:floor/floor.dart';
import 'package:phteven_led_mobile/state/model/Device.dart';

@dao
abstract class DeviceRepository {
  @Query('SELECT * FROM Device')
  Future<List<Device>> findAll();

  @Insert()
  Future<int> insert(Device entity);

  @delete
  Future<int> remove(Device entity);

  @Update(onConflict: OnConflictStrategy.replace)
  Future<void> update(Device entity);
}

import 'dart:async';

import 'package:floor/floor.dart';
import 'package:phteven_led_mobile/services/persistence/repository/DeviceRepository.dart';
import 'package:phteven_led_mobile/services/persistence/repository/FavoriteColorRepository.dart';
import 'package:phteven_led_mobile/state/model/Device.dart';
import 'package:phteven_led_mobile/state/model/FavoriteColor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

part 'database.g.dart'; // the generated code will be there

@Database(version: 1, entities: [Device, FavoriteColor])
abstract class AppDatabase extends FloorDatabase {
  DeviceRepository get deviceDao;

  FavoriteColorRepository get favoriteColorDao;
}

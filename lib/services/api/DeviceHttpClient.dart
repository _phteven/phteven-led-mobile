import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:phteven_led_mobile/state/model/Device.dart';

import 'DeviceColorResponse.dart';
import 'GetDeviceInfoResponse.dart';

class DeviceHttpClient {
  static Future<int> putColor(Device device) async {
    final body = jsonEncode(<String, dynamic>{
      "brightness": device.brightness,
      "segments": device.segments
          .map((e) => {
                "red": e.red - device.redCorrection,
                "green": e.green - device.greenCorrection,
                "blue": e.blue - device.blueCorrection,
                "start": e.start,
                "end": e.end,
              })
          .toList(),
      "version": device.version + 1,
    });

    final headers = <String, String>{
      "Content-Type": "application/json",
    };

    final response = await http.put(_url(device, "/color"), headers: headers, body: body);
    final json = jsonDecode(response.body);

    return json["version"];
  }

  static Future<DeviceColorResponse> getColor(Device device) async {
    final response = await http.get(_url(device, "/color"));
    final json = jsonDecode(response.body);

    return DeviceColorResponse.fromJson(json);
  }

  static Future<GetDeviceInfoResponse> getInfo(Device device) async {
    final response = await http.get(_url(device, "/info"));
    final json = jsonDecode(response.body);

    return GetDeviceInfoResponse.fromJson(json);
  }

  static Future<http.Response> save(Device device) {
    return http.put(_url(device, "/save"));
  }

  static Uri _url(Device device, String path) {
    if (device.address == null) {
      throw Exception("Device is offline.");
    }

    return Uri.http("${device.address}:${device.port}", path);
  }
}

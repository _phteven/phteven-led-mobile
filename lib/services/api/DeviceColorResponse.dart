import 'package:phteven_led_mobile/state/model/Segment.dart';

class DeviceColorResponse {
  final int brightness;
  final int version;
  final List<Segment> segments;

  DeviceColorResponse({required this.brightness, required this.version, required this.segments});

  factory DeviceColorResponse.fromJson(Map<String, dynamic> json) {
    return DeviceColorResponse(
      brightness: json['brightness'],
      version: json['version'],
      segments: (json["segments"] as List<dynamic>).map((e) => Segment.fromJson(e)).toList(),
    );
  }
}

class GetDeviceInfoResponse {
  final String ssid;
  final String mac;
  final int mode;
  final String name;

  GetDeviceInfoResponse._({required this.ssid, required this.mac, required this.mode, required this.name});

  factory GetDeviceInfoResponse.fromJson(Map<String, dynamic> json) {
    return GetDeviceInfoResponse._(
      ssid: json["ssid"],
      mac: json["mac"],
      mode: json["mode"],
      name: json["id"],
    );
  }
}

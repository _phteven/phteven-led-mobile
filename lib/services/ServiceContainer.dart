import 'package:phteven_led_mobile/services/persistence/repository/DeviceRepository.dart';
import 'package:phteven_led_mobile/services/persistence/repository/FavoriteColorRepository.dart';

import 'DeviceUpdateColorThrottling.dart';
import 'net/MdnsService.dart';

class ServiceContainer {
  final DeviceRepository deviceRepository;
  final FavoriteColorRepository favoriteColorRepository;
  final UpdateDeviceColorService updateDeviceColorService;
  final MdnsService mdnsService;

  static ServiceContainer? _instance;

  ServiceContainer({
    required this.deviceRepository,
    required this.favoriteColorRepository,
    required this.updateDeviceColorService,
    required this.mdnsService,
  });

  static void init({
    required DeviceRepository deviceRepository,
    required FavoriteColorRepository favoriteColorRepository,
    required UpdateDeviceColorService updateDeviceColorService,
    required MdnsService mdnsService,
  }) {
    _instance = ServiceContainer(
      deviceRepository: deviceRepository,
      favoriteColorRepository: favoriteColorRepository,
      updateDeviceColorService: updateDeviceColorService,
      mdnsService: mdnsService,
    );
  }

  static ServiceContainer get get {
    return _instance ?? (throw Exception("instance is not initialized"));
  }
}

import 'package:redux/redux.dart';

import 'actions.dart';
import 'model/AppState.dart';

final reducer = combineReducers<AppState>([
  TypedReducer<AppState, FetchCachedDevicesResponseAction>((prevState, action) => prevState.setCachedDevices(devices: action.devices)),
  TypedReducer<AppState, ScanDevicesRequestAction>((prevState, action) => prevState.startScanningDevices(action.showLoader)),
  TypedReducer<AppState, ScanDevicesResponseAction>((prevState, action) => prevState.finishScanningDevices(devices: action.devices)),
  TypedReducer<AppState, SelectDeviceAction>((prevState, action) => prevState.selectDevice(device: action.device)),
  TypedReducer<AppState, DeselectDeviceAction>((prevState, action) => prevState.clearSelectedDevice()),
  TypedReducer<AppState, SelectDeviceSegmentAction>((prevState, action) => prevState.selectDeviceSegment(segment: action.segment)),
  TypedReducer<AppState, PutDeviceColorResponseAction>((prevState, action) => prevState.setDeviceVersion(hostname: action.hostname, version: action.version)),
  TypedReducer<AppState, SetDeviceBrightnessAction>((prevState, action) => prevState.setDeviceBrightness(
        hostname: action.hostname,
        brightness: action.brightness,
      )),
  TypedReducer<AppState, SetDeviceColorCorrectionAction>((prevState, action) => prevState.updateDeviceColorCorrection(action.red, action.green, action.blue)),
  TypedReducer<AppState, RemoveDeviceAction>((prevState, action) => prevState.removeDevice(action.device)),
  TypedReducer<AppState, FetchDeviceColorResponseAction>((prevState, action) => prevState.setDeviceColor(hostname: action.hostname, response: action.response)),
  TypedReducer<AppState, FetchDeviceInfoResponseAction>((prevState, action) => prevState.setDeviceInfo(hostname: action.hostname, response: action.response)),
  TypedReducer<AppState, UpdateDeviceDisplayFormValue>((prevState, action) => prevState.updateDeviceDisplayForm(action.displayName)),
  TypedReducer<AppState, SaveDeviceDisplayNameFormAction>((prevState, action) => prevState.saveDeviceDisplayForm()),
  TypedReducer<AppState, CancelDeviceDisplayNameFormAction>((prevState, action) => prevState.cancelDeviceDisplayForm()),
  TypedReducer<AppState, AddDeviceSegmentAction>((prevState, action) => prevState.addDeviceSegment(device: action.device)),
  TypedReducer<AppState, RemoveDeviceSegmentAction>((prevState, action) => prevState.removeDeviceSegment(device: action.device)),
  TypedReducer<AppState, UpdateDeviceSegmentPositionAction>((prevState, action) => prevState.updateDeviceSegmentPosition(start: action.start, end: action.end)),
  TypedReducer<AppState, UpdateDeviceSegmentColorAction>((prevState, action) => prevState.updateDeviceSegmentColor(
        red: action.red,
        green: action.green,
        blue: action.blue,
      )),
  TypedReducer<AppState, DeviceHttpErrorAction>((prevState, action) => prevState.onDeviceError(exception: action.exception)),
  TypedReducer<AppState, ClearDeviceHttpErrorAction>((prevState, action) => prevState.clearDeviceError()),
  TypedReducer<AppState, FetchFavoriteColorsResponseAction>((prevState, action) => prevState.loadColors(action.colors)),
  TypedReducer<AppState, AddFavoriteColorResponseAction>((prevState, action) => prevState.addFavoriteColor(action.color)),
]);

import 'package:phteven_led_mobile/services/api/DeviceHttpClient.dart';
import 'package:phteven_led_mobile/state/actions.dart';
import 'package:phteven_led_mobile/state/model/AppState.dart';
import 'package:phteven_led_mobile/state/model/FavoriteColor.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

import '../services/ServiceContainer.dart';

bool instanceOf(dynamic obj, List<Type> instances) {
  return instances.where((it) => it == obj.runtimeType).length > 0;
}

Stream<dynamic> _loadCachedDevicesRequestEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
  return actions.whereType<FetchCachedDevicesRequestAction>().asyncMap((action) async {
    final devices = await ServiceContainer.get.deviceRepository.findAll();

    return FetchCachedDevicesResponseAction(devices.toSet());
  });
}

Stream<dynamic> _loadCachedDevicesResponseEpic(Stream<dynamic> actions, EpicStore<AppState> store) async* {
  await for (var action in actions) {
    if (action is FetchCachedDevicesResponseAction) {
      for (var device in store.state.devices) {
        if (device.isOnline) {
          yield FetchDeviceColorRequestAction(device);
          yield FetchDeviceInfoRequestAction(device);
        }
      }
    }
  }
}

Stream<dynamic> _updateDeviceColorEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
  return actions
      .where((action) => instanceOf(action, [
            AddDeviceSegmentAction,
            RemoveDeviceSegmentAction,
            UpdateDeviceSegmentPositionAction,
            UpdateDeviceSegmentColorAction,
            SetDeviceColorCorrectionAction,
          ]))
      .map((action) {
    final device = store.state.selectedDevice;
    if (device != null) {
      return PutDeviceColorRequestAction(device);
    }
  });
}

Stream<dynamic> _setBrightnessEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
  return actions.whereType<SetDeviceBrightnessAction>().map((action) {
    try {
      final device = store.state.devices.firstWhere((it) => it.hostname == action.hostname);

      return PutDeviceColorRequestAction(device);
    } on StateError {}
  });
}

Stream<dynamic> _putDeviceColorRequestEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
  return actions.whereType<PutDeviceColorRequestAction>().map((action) => ServiceContainer.get.updateDeviceColorService.update(action.device));
}

Stream<dynamic> _removeDeviceEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
  return actions.whereType<RemoveDeviceAction>().map((action) => ServiceContainer.get.deviceRepository.remove(action.device));
}

Stream<dynamic> _searchDevicesRequestEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
  return actions.whereType<ScanDevicesRequestAction>().asyncMap((action) async {
    final devices = await ServiceContainer.get.mdnsService.scanDevices();

    devices.forEach((device) {
      try {
        store.state.devices.firstWhere((it) => it.hostname == device.hostname, orElse: null);
      } on StateError {
        try {
          ServiceContainer.get.deviceRepository.insert(device);
        } catch (e) {}
      }
    });

    return ScanDevicesResponseAction(devices);
  });
}

Stream<dynamic> _scanDevicesResponseEpic(Stream<dynamic> actions, EpicStore<AppState> store) async* {
  await for (var action in actions) {
    if (action is ScanDevicesResponseAction) {
      for (var device in action.devices) {
        yield FetchDeviceColorRequestAction(device);
        yield FetchDeviceInfoRequestAction(device);
      }
    }
  }
}

Stream<dynamic> _fetchDeviceInfoRequestEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
  return actions.whereType<FetchDeviceInfoRequestAction>().asyncMap((action) async {
    try {
      final response = await DeviceHttpClient.getInfo(action.device);

      return FetchDeviceInfoResponseAction(action.device.hostname, response);
    } on Exception catch (e) {
      return DeviceHttpErrorAction(e);
    }
  });
}

Stream<dynamic> _fetchDeviceInfoResponseEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
  return actions.whereType<FetchDeviceInfoResponseAction>().map((action) {
    try {
      final device = store.state.devices.firstWhere((it) => it.hostname == action.hostname);

      ServiceContainer.get.deviceRepository.update(device);
    } on StateError {}
  });
}

Stream<dynamic> _fetchDeviceColorEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
  return actions.whereType<FetchDeviceColorRequestAction>().asyncMap((action) async {
    try {
      final response = await DeviceHttpClient.getColor(action.device);

      return FetchDeviceColorResponseAction(action.device.hostname, response);
    } on Exception catch (e) {
      return DeviceHttpErrorAction(e);
    }
  });
}

Stream<dynamic> _deviceSaveEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
  return actions.whereType<SaveSelectedDeviceRequestAction>().asyncMap((action) async {
    final device = action.device;

    try {
      await DeviceHttpClient.save(device);

      return SaveSelectedDeviceResponseAction();
    } on Exception catch (e) {
      return DeviceHttpErrorAction(e);
    }
  });
}

Stream<dynamic> _setDisplayNameEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
  return actions.whereType<SaveDeviceDisplayNameFormAction>().map((action) {
    try {
      final device = store.state.devices.firstWhere((it) => it.hostname == store.state.deviceDisplayNameForm.hostname);

      ServiceContainer.get.deviceRepository.update(device);
    } on StateError {}
  });
}

Stream<dynamic> _setDeviceColorCorrectionEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
  return actions.whereType<SetDeviceColorCorrectionAction>().map((action) {
    try {
      final device = store.state.devices.firstWhere((it) => it.hostname == store.state.deviceColorCorrectionForm.hostname);

      ServiceContainer.get.deviceRepository.update(device);
    } on StateError {}
  });
}

Stream<dynamic> _loadFavoriteColorsEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
  return actions.whereType<FetchFavoriteColorsRequestAction>().map((action) async {
    final colors = await ServiceContainer.get.favoriteColorRepository.findAll();

    return FetchFavoriteColorsResponseAction(colors);
  });
}

Stream<dynamic> _addFavoriteColorsEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
  return actions.whereType<AddFavoriteColorRequestAction>().map((action) async {
    final color = FavoriteColor(0, action.r, action.g, action.b);
    await ServiceContainer.get.favoriteColorRepository.insert(color);

    return AddFavoriteColorResponseAction(color);
  });
}

final epics = combineEpics<AppState>([
  _loadCachedDevicesRequestEpic,
  _loadCachedDevicesResponseEpic,
  _updateDeviceColorEpic,
  _setBrightnessEpic,
  _putDeviceColorRequestEpic,
  _removeDeviceEpic,
  _searchDevicesRequestEpic,
  _scanDevicesResponseEpic,
  _fetchDeviceInfoRequestEpic,
  _fetchDeviceColorEpic,
  _deviceSaveEpic,
  _fetchDeviceInfoResponseEpic,
  _setDisplayNameEpic,
  _setDeviceColorCorrectionEpic,
  _loadFavoriteColorsEpic,
  _addFavoriteColorsEpic,
]);

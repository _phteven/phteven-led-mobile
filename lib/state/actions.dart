import 'package:flutter/foundation.dart';
import 'package:phteven_led_mobile/services/api/DeviceColorResponse.dart';
import 'package:phteven_led_mobile/services/api/GetDeviceInfoResponse.dart';
import 'package:phteven_led_mobile/state/model/Device.dart';
import 'package:phteven_led_mobile/state/model/FavoriteColor.dart';

import 'model/Segment.dart';

@immutable
class SelectDeviceAction {
  final Device device;

  SelectDeviceAction(this.device);
}

@immutable
class DeselectDeviceAction {}

@immutable
class SelectDeviceSegmentAction {
  final Segment segment;

  SelectDeviceSegmentAction(this.segment);
}

@immutable
class AddDeviceSegmentAction {
  final Device device;

  AddDeviceSegmentAction(this.device);
}

@immutable
class RemoveDeviceSegmentAction {
  final Device device;

  RemoveDeviceSegmentAction(this.device);
}

@immutable
class UpdateDeviceSegmentPositionAction {
  final int start;
  final int end;

  UpdateDeviceSegmentPositionAction(this.start, this.end);
}

@immutable
class UpdateDeviceSegmentColorAction {
  final int red;
  final int green;
  final int blue;

  UpdateDeviceSegmentColorAction(this.red, this.green, this.blue);
}

@immutable
class RemoveDeviceAction {
  final Device device;

  RemoveDeviceAction(this.device);
}

@immutable
class FetchCachedDevicesRequestAction {}

@immutable
class FetchCachedDevicesResponseAction {
  final Set<Device> devices;

  FetchCachedDevicesResponseAction(this.devices);
}

@immutable
class ScanDevicesRequestAction {
  final bool showLoader;

  ScanDevicesRequestAction(this.showLoader);
}

@immutable
class ScanDevicesResponseAction {
  final Set<Device> devices;

  ScanDevicesResponseAction(this.devices);
}

@immutable
class SetDeviceBrightnessAction {
  final String hostname;
  final int brightness;

  SetDeviceBrightnessAction(this.hostname, this.brightness);
}

@immutable
class SaveSelectedDeviceRequestAction {
  final Device device;

  SaveSelectedDeviceRequestAction(this.device);
}

@immutable
class SaveSelectedDeviceResponseAction {}

@immutable
class FetchDeviceColorRequestAction {
  final Device device;

  FetchDeviceColorRequestAction(this.device);
}

@immutable
class FetchDeviceColorResponseAction {
  final String hostname;
  final DeviceColorResponse response;

  FetchDeviceColorResponseAction(this.hostname, this.response);
}

@immutable
class FetchDeviceInfoRequestAction {
  final Device device;

  FetchDeviceInfoRequestAction(this.device);
}

@immutable
class FetchDeviceInfoResponseAction {
  final String hostname;
  final GetDeviceInfoResponse response;

  FetchDeviceInfoResponseAction(this.hostname, this.response);
}

@immutable
class UpdateDeviceDisplayFormValue {
  final String displayName;

  UpdateDeviceDisplayFormValue(this.displayName);
}

@immutable
class SaveDeviceDisplayNameFormAction {}

@immutable
class CancelDeviceDisplayNameFormAction {}

@immutable
class PutDeviceColorRequestAction {
  final Device device;

  PutDeviceColorRequestAction(this.device);
}

@immutable
class SetDeviceColorCorrectionAction {
  final int red;
  final int green;
  final int blue;

  SetDeviceColorCorrectionAction({required this.red, required this.green, required this.blue});
}

@immutable
class PutDeviceColorResponseAction {
  final String hostname;
  final int version;

  PutDeviceColorResponseAction(this.hostname, this.version);
}

@immutable
class DeviceHttpErrorAction {
  final Exception exception;

  DeviceHttpErrorAction(this.exception);
}

@immutable
class ClearDeviceHttpErrorAction {}

@immutable
class FetchFavoriteColorsRequestAction {}

@immutable
class FetchFavoriteColorsResponseAction {
  final List<FavoriteColor> colors;

  FetchFavoriteColorsResponseAction(this.colors);
}

@immutable
class AddFavoriteColorRequestAction {
  final int r;
  final int g;
  final int b;

  AddFavoriteColorRequestAction(this.r, this.g, this.b);
}

@immutable
class AddFavoriteColorResponseAction {
  final FavoriteColor color;

  AddFavoriteColorResponseAction(this.color);
}

@immutable
class RemoveFavoriteColorAction {
  final int id;

  RemoveFavoriteColorAction(this.id);
}

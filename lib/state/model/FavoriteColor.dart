import 'package:equatable/equatable.dart';
import 'package:floor/floor.dart';
import 'package:flutter/foundation.dart';

@entity
@immutable
class FavoriteColor extends Equatable {
  @PrimaryKey(autoGenerate: true)
  @ColumnInfo(name: "id")
  final int id;

  @ColumnInfo(name: "red")
  final int red;

  @ColumnInfo(name: "green")
  final int green;

  @ColumnInfo(name: "blue")
  final int blue;

  FavoriteColor(this.id, this.red, this.green, this.blue);

  @override
  List<Object?> get props => [id];
}

import 'package:flutter/foundation.dart';

@immutable
class Segment {
  final int red;
  final int green;
  final int blue;
  final int start;
  final int end;

  static const MAX_SEGMENTS = 6;
  static const MIN = 0;
  static const MAX = 99;
  static const MIN_WIDTH = 15;

  Segment({required this.red, required this.green, required this.blue, required this.start, required this.end});

  Segment copyWith({int? red, int? green, int? blue, int? start, int? end}) {
    return Segment(
      red: red ?? this.red,
      green: green ?? this.green,
      blue: blue ?? this.blue,
      start: start ?? this.start,
      end: end ?? this.end,
    );
  }

  factory Segment.fromJson(Map<String, dynamic> json) {
    return Segment(
      red: json['red'],
      green: json['green'],
      blue: json['blue'],
      start: json['start'],
      end: json['end'],
    );
  }

  int width() => end - start;
}

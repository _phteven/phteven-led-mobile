import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:phteven_led_mobile/extensions.dart';
import 'package:phteven_led_mobile/services/api/DeviceColorResponse.dart';
import 'package:phteven_led_mobile/services/api/GetDeviceInfoResponse.dart';
import 'package:phteven_led_mobile/state/model/Device.dart';
import 'package:phteven_led_mobile/state/model/FavoriteColor.dart';

import 'DeviceColorCorrectionForm.dart';
import 'DeviceDisplayNameForm.dart';
import 'Segment.dart';

@immutable
class AppState {
  final Set<Device> devices;
  final List<FavoriteColor> colors;
  final Exception? deviceHttpError;
  final bool deviceLoaderVisible;
  final Device? selectedDevice;
  final Segment? selectedDeviceSegment;
  final DeviceDisplayNameForm deviceDisplayNameForm;
  final DeviceColorCorrectionForm deviceColorCorrectionForm;

  AppState({
    required this.devices,
    required this.colors,
    required this.deviceHttpError,
    required this.selectedDevice,
    required this.selectedDeviceSegment,
    required this.deviceLoaderVisible,
    required this.deviceDisplayNameForm,
    required this.deviceColorCorrectionForm,
  });

  factory AppState.initial() => AppState(
        devices: {},
        colors: [],
        deviceHttpError: null,
        deviceLoaderVisible: false,
        selectedDevice: null,
        selectedDeviceSegment: null,
        deviceDisplayNameForm: DeviceDisplayNameForm.create(),
        deviceColorCorrectionForm: DeviceColorCorrectionForm.create(),
      );

  AppState clearSelectedDevice() => AppState(
        devices: devices,
        colors: [],
        deviceHttpError: null,
        selectedDevice: null,
        selectedDeviceSegment: null,
        deviceLoaderVisible: deviceLoaderVisible,
        deviceDisplayNameForm: deviceDisplayNameForm.clear(),
        deviceColorCorrectionForm: deviceColorCorrectionForm.clear(),
      );

  AppState removeDevice(Device device) {
    return this._copyWith(
      selectedDevice: device == selectedDevice ? null : selectedDevice,
      selectedDeviceSegment: device == selectedDevice ? null : selectedDeviceSegment,
      devices: devices.where((it) => it != device).toSet(),
    );
  }

  AppState selectDevice({required Device device}) {
    return _copyWith(
      selectedDevice: device,
      selectedDeviceSegment: device.segments.first,
      deviceDisplayNameForm: deviceDisplayNameForm.setDevice(hostname: device.hostname, displayName: device.getTitle()),
      deviceColorCorrectionForm: deviceColorCorrectionForm.setDevice(device: device),
    );
  }

  AppState selectDeviceSegment({required Segment segment}) {
    return _copyWith(selectedDeviceSegment: segment);
  }

  AppState startScanningDevices(bool showLoader) {
    return _copyWith(
      searchingDevices: showLoader,
    );
  }

  AppState finishScanningDevices({required Set<Device> devices}) {
    final updatedDevices = devices.map((newDevice) {
      final device = this.devices.firstWhere(
            (it) => it.hostname == newDevice.hostname,
            orElse: () => newDevice,
          );

      final address = newDevice.address;
      final port = newDevice.port;

      return address != null && port != null ? device.setOnline(address, port) : device;
    }).toSet();

    final union = updatedDevices.union(this.devices);

    return _copyWith(searchingDevices: false, devices: union);
  }

  AppState setCachedDevices({required Set<Device> devices}) {
    if (this.devices.isNotEmpty) {
      return this;
    }

    return _copyWith(devices: devices);
  }

  AppState setDeviceVersion({required String hostname, required int version}) {
    final device = devices.find((it) => it.hostname == hostname);

    return _updateDevice(
      oldDevice: device,
      newDevice: device?.setVersion(version),
    );
  }

  AppState setDeviceBrightness({required String hostname, required int brightness}) {
    final device = devices.find((it) => it.hostname == hostname);

    return _updateDevice(
      oldDevice: device,
      newDevice: device?.setBrightness(brightness),
    );
  }

  AppState setDeviceColor({required String hostname, required DeviceColorResponse response}) {
    final device = devices.find((it) => it.hostname == hostname);

    if (device == null) {
      return this;
    }

    final correctedSegments = response.segments
        .map(
          (e) => e.copyWith(
            red: e.red > 0 ? (e.red + device.redCorrection) : 0,
            green: e.green > 0 ? (e.green + device.greenCorrection) : 0,
            blue: e.blue > 0 ? (e.blue + device.blueCorrection) : 0,
          ),
        )
        .toList();

    return _updateDevice(
      oldDevice: device,
      newDevice: device.setColor(response.brightness, response.version, correctedSegments),
    );
  }

  AppState setDeviceInfo({required String hostname, required GetDeviceInfoResponse response}) {
    final device = devices.find((it) => it.hostname == hostname);
    final displayName = device?.displayName ?? response.name;

    return _updateDevice(
      oldDevice: device,
      newDevice: device?.setInfo(
        displayName,
        response.name,
        response.mac,
        response.ssid,
        response.mode == 1 ? Device.DeviceModeNetwork : Device.DeviceModeAP,
      ),
    );
  }

  AppState updateDeviceDisplayForm(String displayName) {
    return _copyWith(deviceDisplayNameForm: deviceDisplayNameForm.setText(displayName: displayName));
  }

  AppState updateDeviceColorCorrection(int red, int green, int blue) {
    final hostname = deviceColorCorrectionForm.hostname;
    final device = devices.find((it) => it.hostname == hostname);

    return _updateDevice(
      oldDevice: device,
      newDevice: device?.setColorCorrection(red, green, blue),
    )._copyWith(deviceColorCorrectionForm: deviceColorCorrectionForm.update(red: red, green: green, blue: blue));
  }

  AppState saveDeviceDisplayForm() {
    final hostname = deviceDisplayNameForm.hostname;
    final displayName = deviceDisplayNameForm.displayName;
    final device = devices.find((it) => it.hostname == hostname);

    return _updateDevice(
      oldDevice: device,
      newDevice: device?.setDisplayName(displayName),
    );
  }

  AppState cancelDeviceDisplayForm() {
    final hostname = deviceDisplayNameForm.hostname;
    final device = devices.find((it) => it.hostname == hostname);

    if (device != null) {
      return _copyWith(deviceDisplayNameForm: deviceDisplayNameForm.setText(displayName: device.getTitle()));
    }

    return this;
  }

  AppState addDeviceSegment({required Device device}) {
    final distribution = Segment.MAX / (device.segments.length + 1);

    final redistributedSegments = device.segments
        .mapIndexed(
          (index, e) => e.copyWith(
            start: (index * distribution).toInt(),
            end: ((index * distribution) + distribution).toInt(),
          ),
        )
        .toList();

    redistributedSegments.add(
      Segment(
        red: redistributedSegments.last.red,
        green: redistributedSegments.last.green,
        blue: redistributedSegments.last.blue,
        start: (redistributedSegments.length * distribution).toInt(),
        end: (redistributedSegments.length * distribution + distribution).toInt(),
      ),
    );

    redistributedSegments.forEach((element) {
      print("${element.start}/${element.end}");
    });

    final newDevice = device.setSegments(redistributedSegments);

    return _updateDevice(
      oldDevice: selectedDevice,
      newDevice: newDevice,
    )._copyWith(selectedDeviceSegment: redistributedSegments[redistributedSegments.length - 1]);
  }

  AppState removeDeviceSegment({required Device device}) {
    final distribution = 100 / (device.segments.length - 1);
    final redistributedSegments = device.segments
        .mapIndexed((index, e) => e.copyWith(
              start: (index * distribution).toInt(),
              end: min((index * distribution) + distribution, Segment.MAX).toInt(),
            ))
        .toList();

    redistributedSegments.removeLast();

    final selectedDeviceSegment = this.selectedDeviceSegment;
    int selectedDeviceIndex = selectedDeviceSegment != null ? device.segments.indexOf(selectedDeviceSegment) : 0;
    if (selectedDeviceIndex == device.segments.length - 1) {
      selectedDeviceIndex--;
    }

    final newDevice = device.setSegments(redistributedSegments);
    final newSelectedDeviceSegment = redistributedSegments[selectedDeviceIndex];

    redistributedSegments.forEach((element) {
      print("${element.start}/${element.end}");
    });

    return _updateDevice(oldDevice: device, newDevice: newDevice)._copyWith(selectedDeviceSegment: newSelectedDeviceSegment);
  }

  AppState updateDeviceSegmentPosition({required int start, required int end}) {
    final selectedDevice = this.selectedDevice;
    final selectedDeviceSegment = this.selectedDeviceSegment;

    if (selectedDevice == null) return this;
    if (selectedDeviceSegment == null) return this;

    final segments = selectedDevice.segments;
    final index = selectedDevice.segments.indexOf(selectedDeviceSegment);

    if (index == -1) return this;

    // Left handle was dragged
    if (start < selectedDeviceSegment.start || start > selectedDeviceSegment.start) {
      if (index == 0) return this; // First segment

      final previousElement = segments[index - 1];

      if (start < previousElement.start + Segment.MIN_WIDTH) return this;

      if (start > selectedDeviceSegment.end - Segment.MIN_WIDTH) return this;

      segments[index - 1] = previousElement.copyWith(end: start);
    }

    // Right handle was dragged
    if (end > selectedDeviceSegment.end || end < selectedDeviceSegment.end) {
      if (index == segments.length - 1) return this; // Last segment

      final nextElement = segments[index + 1];

      if (end > nextElement.end - Segment.MIN_WIDTH) return this;

      if (end < selectedDeviceSegment.start + Segment.MIN_WIDTH) return this;

      segments[index + 1] = nextElement.copyWith(start: end);
    }

    final newSelectedDeviceSegment = selectedDeviceSegment.copyWith(start: start, end: end);
    final newSegments = segments.map((e) {
      if (e == selectedDeviceSegment) return newSelectedDeviceSegment;

      return e;
    }).toList();

    return _updateDevice(
      oldDevice: selectedDevice,
      newDevice: selectedDevice.setSegments(newSegments),
    )._copyWith(
      selectedDeviceSegment: newSelectedDeviceSegment,
    );
  }

  AppState loadColors(List<FavoriteColor> colors) {
    return _copyWith(
      colors: colors,
    );
  }

  AppState addFavoriteColor(FavoriteColor color) {
    return _copyWith(
      colors: [...colors, color],
    );
  }

  AppState updateDeviceSegmentColor({required int red, required int green, required int blue}) {
    final selectedDeviceSegment = this.selectedDeviceSegment;
    final selectedDevice = this.selectedDevice;

    if (selectedDeviceSegment == null) return this;
    if (selectedDevice == null) return this;

    final newSelectedDeviceSegment = selectedDeviceSegment.copyWith(red: red, green: green, blue: blue);
    final segments = selectedDevice.segments.map((e) {
      if (e == this.selectedDeviceSegment) {
        return newSelectedDeviceSegment;
      }

      return e;
    }).toList();

    final newDevice = selectedDevice.setSegments(segments);

    return _updateDevice(
      oldDevice: selectedDevice,
      newDevice: newDevice,
    )._copyWith(selectedDeviceSegment: newSelectedDeviceSegment);
  }

  AppState onDeviceError({required Exception exception}) {
    return _copyWith(
      deviceHttpError: exception,
    );
  }

  AppState clearDeviceError() {
    return AppState(
      devices: devices,
      colors: colors,
      deviceHttpError: null,
      selectedDevice: selectedDevice,
      selectedDeviceSegment: selectedDeviceSegment,
      deviceLoaderVisible: deviceLoaderVisible,
      deviceDisplayNameForm: deviceDisplayNameForm,
      deviceColorCorrectionForm: deviceColorCorrectionForm,
    );
  }

  AppState _copyWith({
    Set<Device>? devices,
    List<FavoriteColor>? colors,
    Exception? deviceHttpError,
    Device? selectedDevice,
    Segment? selectedDeviceSegment,
    bool? searchingDevices,
    DeviceDisplayNameForm? deviceDisplayNameForm,
    DeviceColorCorrectionForm? deviceColorCorrectionForm,
  }) =>
      AppState(
        devices: devices ?? this.devices,
        colors: colors ?? this.colors,
        deviceHttpError: deviceHttpError ?? this.deviceHttpError,
        selectedDevice: selectedDevice ?? this.selectedDevice,
        selectedDeviceSegment: selectedDeviceSegment ?? this.selectedDeviceSegment,
        deviceLoaderVisible: searchingDevices ?? this.deviceLoaderVisible,
        deviceDisplayNameForm: deviceDisplayNameForm ?? this.deviceDisplayNameForm,
        deviceColorCorrectionForm: deviceColorCorrectionForm ?? this.deviceColorCorrectionForm,
      );

  AppState _updateDevice({required Device? oldDevice, required Device? newDevice}) {
    if (newDevice == null) {
      return this;
    }

    final isSameDevice = oldDevice == selectedDevice;

    return _copyWith(
      selectedDevice: isSameDevice ? newDevice : selectedDevice,
      selectedDeviceSegment: isSameDevice ? newDevice.segments[0] : selectedDeviceSegment,
      devices: devices.map((e) {
        if (e == oldDevice) {
          return newDevice;
        }

        return e;
      }).toSet(),
    );
  }
}

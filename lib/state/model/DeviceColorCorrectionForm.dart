import 'package:flutter/foundation.dart';

import 'Device.dart';

@immutable
class DeviceColorCorrectionForm {
  final String hostname;
  final int red;
  final int green;
  final int blue;

  DeviceColorCorrectionForm({
    required this.hostname,
    required this.red,
    required this.green,
    required this.blue,
  });

  factory DeviceColorCorrectionForm.create() => DeviceColorCorrectionForm(
        hostname: "",
        red: 255,
        green: 255,
        blue: 255,
      );

  DeviceColorCorrectionForm clear() {
    return DeviceColorCorrectionForm.create();
  }

  DeviceColorCorrectionForm setDevice({
    required Device device,
  }) =>
      DeviceColorCorrectionForm(
        hostname: device.hostname,
        red: 255 - device.redCorrection,
        green: 255 - device.greenCorrection,
        blue: 255 - device.blueCorrection,
      );

  update({required int red, required int green, required int blue}) {
    return DeviceColorCorrectionForm(
      hostname: hostname,
      red: red,
      green: green,
      blue: blue,
    );
  }
}

import 'package:flutter/foundation.dart';

@immutable
class DeviceDisplayNameForm {
  final String hostname;
  final String displayName;

  DeviceDisplayNameForm({required this.hostname, required this.displayName});

  factory DeviceDisplayNameForm.create() => DeviceDisplayNameForm(hostname: "", displayName: "");

  DeviceDisplayNameForm setDevice({
    required String hostname,
    required String displayName,
  }) =>
      DeviceDisplayNameForm(hostname: hostname, displayName: displayName);

  DeviceDisplayNameForm setText({required String displayName}) => DeviceDisplayNameForm(hostname: hostname, displayName: displayName);

  DeviceDisplayNameForm clear() => DeviceDisplayNameForm(hostname: "", displayName: "");
}

import 'package:equatable/equatable.dart';
import 'package:floor/floor.dart';
import 'package:flutter/foundation.dart';

import 'Segment.dart';

@entity
@immutable
class Device extends Equatable {
  @PrimaryKey()
  @ColumnInfo(name: "host_name")
  final String hostname;

  @ColumnInfo(name: "brightness")
  final int brightness;

  @ignore
  final List<Segment> segments;

  @ignore
  final int version;

  @ColumnInfo(name: "address")
  final String? address;

  @ColumnInfo(name: "port")
  final int? port;

  @ColumnInfo(name: "hardware_name")
  final String? hardwareName;

  @ColumnInfo(name: "display_name")
  final String? displayName;

  @ColumnInfo(name: "mode")
  final String? mode;

  @ColumnInfo(name: "ssid")
  final String? ssid;

  @ColumnInfo(name: "mac")
  final String? mac;

  @ColumnInfo(name: "red_correction")
  final int redCorrection;

  @ColumnInfo(name: "green_correction")
  final int greenCorrection;

  @ColumnInfo(name: "blue_correction")
  final int blueCorrection;

  @ignore
  List<Object> get props => [hostname];

  @ignore
  bool get isOnline => address != null && port != null;

  @ignore
  bool get isDark => brightness == 0;

  static const String DeviceModeNetwork = "NETWORK";
  static const String DeviceModeAP = "AP";

  Device({
    required this.hostname,
    List<Segment>? segments,
    int? brightness,
    int? version,
    this.address,
    this.port,
    this.displayName,
    this.hardwareName,
    this.mode,
    this.ssid,
    this.mac,
    required this.redCorrection,
    required this.greenCorrection,
    required this.blueCorrection,
  })  : segments = segments ?? List.empty(growable: false),
        brightness = brightness ?? 0,
        version = version ?? 0;

  Device setOnline(String address, int port) {
    return _copyWith(
      address: address,
      port: port,
    );
  }

  Device setOffline() {
    return Device(
      hostname: hostname,
      segments: segments,
      version: version,
      hardwareName: hardwareName,
      address: null,
      port: null,
      brightness: brightness,
      displayName: displayName,
      mode: mode,
      ssid: ssid,
      mac: mac,
      redCorrection: redCorrection,
      blueCorrection: blueCorrection,
      greenCorrection: greenCorrection,
    );
  }

  Device setVersion(int version) {
    return _copyWith(version: version);
  }

  Device setBrightness(int brightness) {
    return _copyWith(brightness: brightness);
  }

  Device setDisplayName(String displayName) {
    return _copyWith(displayName: displayName);
  }

  Device setColorCorrection(int red, int green, int blue) {
    return _copyWith(
      redCorrection: 255 - red,
      greenCorrection: 255 - green,
      blueCorrection: 255 - blue,
    );
  }

  Device setSegments(List<Segment> segments) {
    return _copyWith(
      segments: segments,
    );
  }

  Device setColor(int brightness, int version, List<Segment> segments) {
    return setBrightness(brightness).setVersion(version).setSegments(segments);
  }

  Device setInfo(
    String displayName,
    String hardwareName,
    String mac,
    String ssid,
    String mode,
  ) {
    return _copyWith(
      displayName: displayName,
      hardwareName: hardwareName,
      mac: mac,
      ssid: ssid,
      mode: mode,
    );
  }

  String getTitle() {
    final displayName = this.displayName;
    final hardwareName = this.hardwareName;

    if (displayName != null) {
      return displayName;
    }

    if (hardwareName != null) {
      return hardwareName;
    }

    return hostname;
  }

  bool isAccessPointMode() {
    return mode == DeviceModeAP;
  }

  Device _copyWith({
    String? hostname,
    List<Segment>? segments,
    int? version,
    int? brightness,
    String? address,
    int? port,
    int? redCorrection,
    int? blueCorrection,
    int? greenCorrection,
    String? displayName,
    String? hardwareName,
    String? mode,
    String? ssid,
    String? mac,
  }) {
    return Device(
      hostname: hostname ?? this.hostname,
      segments: segments ?? this.segments,
      version: version ?? this.version,
      hardwareName: hardwareName ?? this.hardwareName,
      address: address ?? this.address,
      port: port ?? this.port,
      brightness: brightness ?? this.brightness,
      displayName: displayName ?? this.displayName,
      redCorrection: redCorrection ?? this.redCorrection,
      blueCorrection: blueCorrection ?? this.blueCorrection,
      greenCorrection: greenCorrection ?? this.greenCorrection,
      mode: mode ?? this.mode,
      ssid: ssid ?? this.ssid,
      mac: mac ?? this.mac,
    );
  }
}

import 'dart:io';

import 'package:device_info/device_info.dart';
import "package:flutter/material.dart";
import 'package:flutter/services.dart';
import "package:phteven_led_mobile/services/DeviceUpdateColorThrottling.dart";
import 'package:phteven_led_mobile/services/net/AbstractMdnsEngine.dart';
import 'package:phteven_led_mobile/services/net/MdnsEngine.dart';
import 'package:phteven_led_mobile/services/net/MdnsService.dart';
import 'package:phteven_led_mobile/services/net/MockedMDnsEngine.dart';
import 'package:phteven_led_mobile/services/persistence/database.dart';
import 'package:phteven_led_mobile/services/persistence/repository/InMemoryDeviceRepository.dart';
import 'package:phteven_led_mobile/services/persistence/repository/InMemoryFavoriteColorRepository.dart';
import "package:phteven_led_mobile/state/actions.dart";
import "package:phteven_led_mobile/state/epics.dart";
import "package:phteven_led_mobile/state/model/AppState.dart";
import "package:phteven_led_mobile/state/reducer.dart";
import 'package:phteven_led_mobile/widgets/App.dart';
import "package:redux/redux.dart";
import "package:redux_epics/redux_epics.dart";

import "services/ServiceContainer.dart";

loggerMiddleware(Store<AppState> store, action, NextDispatcher next) {
  print("Redux action: $action");
  next(action);
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final deviceInfoPlugin = DeviceInfoPlugin();
  bool isPhysicalDevice = false;

  try {
    if (Platform.isAndroid) {
      isPhysicalDevice = (await deviceInfoPlugin.androidInfo).isPhysicalDevice;
    } else if (Platform.isIOS) {
      isPhysicalDevice = (await deviceInfoPlugin.iosInfo).isPhysicalDevice;
    }
  } on PlatformException {}

  final store = Store<AppState>(
    reducer,
    middleware: [loggerMiddleware, EpicMiddleware(epics)],
    initialState: AppState.initial(),
  );

  final database = await $FloorAppDatabase.databaseBuilder("app_database.db").build();
  final mDnsName = "_phteven-led._tcp.local";

  final AbstractMdnsEngine mdnsEngine = isPhysicalDevice ? MdnsEngine(name: mDnsName) : MockedDnsEngine();

  ServiceContainer.init(
    deviceRepository: isPhysicalDevice ? database.deviceDao : InMemoryDeviceRepository(),
    favoriteColorRepository: isPhysicalDevice ? database.favoriteColorDao : InMemoryFavoriteColorRepository(),
    updateDeviceColorService: UpdateDeviceColorService(
      (device, version) => store.dispatch(PutDeviceColorResponseAction(device.hostname, version)),
      (e) {
        print("error: $e");
        store.dispatch(DeviceHttpErrorAction(e));
      },
    ),
    mdnsService: MdnsService(engine: mdnsEngine),
  );

  runApp(App(store: store));
}

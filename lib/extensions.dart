extension ListExtensions<E> on List<E> {
  Iterable<T> mapIndexed<T>(T f(int index, E element)) {
    return this.asMap().map((key, value) {
      final mapped = f(key, value);

      return MapEntry(key, mapped);
    }).values;
  }
}

extension IterableExtensions<E> on Iterable<E> {
  E? find(bool test(E element)) {
    try {
      return firstWhere(test);
    } on StateError {
      return null;
    }
  }
}
